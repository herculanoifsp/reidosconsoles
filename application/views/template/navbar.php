<header>
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark <?= $rodape->color_site ?> darken-1 scrolling-navbar">
        <a class="navbar-brand" href="<?= base_url("Home") ?>"><img src="<?= base_url("assets/img/Logo/logo_transparent.png") ?>" height="30" alt="Logo da Rei dos Consoles" /></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="<?= base_url("Home") ?>">Home <span class="sr-only">(current)</span></a>
                </li>
                <?php foreach($navbar as $item): ?>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="<?= $item->nome ?>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?= $item->nome ?></a>
                    <div class="dropdown-menu dropdown-primary" aria-labelledby="<?= $item->nome ?>">
                        <?php foreach($item->filhos as $opcoes): ?>
                        <a class="dropdown-item" href="<?= base_url("Home/lista_produtos/$item->nome/$opcoes->nome") ?>"><?= $opcoes->nome ?></a>
                        <?php endforeach; ?>
                    </div>
                </li>
                <?php endforeach; ?>
            </ul>
            <ul class="navbar-nav nav-flex-icons">
                <li class="nav-item">
                    <a href="<?= $rodape->facebook ?>" class="nav-link"><i class="fab fa-facebook-f"></i></a>
                </li>
                <li class="nav-item">
                    <a href="<?= $rodape->twitter ?>" class="nav-link"><i class="fab fa-twitter"></i></a>
                </li>
                <li class="nav-item">
                    <a href="<?= $rodape->instagram ?>" class="nav-link"><i class="fab fa-instagram"></i></a>
                </li>
                <li class="nav-item">
                    <a href="<?= base_url("Login/form_login") ?>" class="nav-link"><i class="fas fa-users"></i></a>
                </li>
            </ul>
        </div>
    </nav>
</header>
<br/>
