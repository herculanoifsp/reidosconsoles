<footer class="page-footer font-small <?= $rodape->color_site ?> darken-4 pt-4">
    <div class="container-fluid text-center text-md-left">
        <div class="row">
			<div class="col-md-4 mt-md-0 mt-4">
              <h5 class="text-uppercase">Sobre a Loja</h5>
              <p><?= $rodape->sobre ?></p>
            </div>
            <div class="col-md-4 mb-md-0 mb-4">
                <h5 class="text-uppercase">Social</h5>
                <ul class="list-unstyled">
                    <li>
                        <a href="<?= $rodape->facebook ?>"><i class="fab fa-facebook-square"></i> Facebook</a>
                    </li>
                    <li>
                        <a href="<?= $rodape->twitter ?>"><i class="fab fa-twitter"></i> Twitter</a>
                    </li>
                    <li>
                        <a href="<?= $rodape->instagram ?>"><i class="fab fa-instagram"></i> Instagram</a>
                    </li>
                </ul>
            </div>
			<hr class="clearfix w-100 d-md-none pb-3">
            <div class="col-md-4 mb-md-0 mb-4">
                <h5 class="text-uppercase">Contatos</h5>
                <ul class="list-unstyled">
                    <li>
                        <p><i class="fas fa-clock fa-lg"></i> <?= $rodape->horario ?></p>
                    </li>
                    <li>
                        <p><i class="fas fa-phone fa-lg"></i> <?= $rodape->telefone ?></p>
                    </li>
                    <li>
                        <p><i class="fas fa-envelope fa-lg"></i> <?= $rodape->email ?></p>
                    </li>
                    <li>
                        <p><i class="fab fa-whatsapp fa-lg"></i> <?= $rodape->celular ?></p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-copyright text-center py-3">© 2019 Copyright:
        <a href="#"> ReidosConsoles.com</a>
    </div>
</footer>
