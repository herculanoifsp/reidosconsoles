<div class="clearfix mt-5 mb-5">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <h3 class="section-heading h1 mb-5">Destaques</h3>
                <hr class="my-5">
                <br/>
                <div class="row">
                <?= $card_destaque ?>
                </div>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <h3 class="section-heading h1 mb-5">Lançamentos</h3>
                <hr class="my-5">
                <br/>
                <div class="row">
                <?= $card_lancamento ?>
                </div>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <h3 class="section-heading h1 mb-5">Promoções</h3>
                <hr class="my-5">
                <br/>
                <div class="row">
                <?= $card_promocao ?>
                </div>
            </div>
        </div>
    </div>
</div>