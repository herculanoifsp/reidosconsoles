<div class="clearfix mt-5 mb-5">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <h4 class="section-heading h2 mb-5"><?= $produto->nome ?></h4>
                <hr class="my-5">
                <br/>
                <div class="row">
                    <div class="col-md-5 col-sm-5 col-xs-12">
                        <div id="carousel-example-1z" class="carousel slide carousel-fade" data-ride="carousel">
                            <div class="carousel-inner" role="listbox">
                                <?php foreach($imagem as $key => $item): ?>
                                <div class="carousel-item <?= $key == 0 ? "active" : "" ?>">
                                    <img class="d-block w-100" src="<?= base_url("assets/img/$item->img") ?>" alt="First slide">
                                </div>
                                <?php endforeach; ?>
                            </div>
                            <a class="carousel-control-prev" href="#carousel-example-1z" role="button" data-slide="prev">
                              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                              <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carousel-example-1z" role="button" data-slide="next">
                              <span class="carousel-control-next-icon" aria-hidden="true"></span>
                              <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-7 col-sm-7 col-xs-12">
                        <?php for($i=0; $i<$produto->estrela;$i++): ?>
                        <i class="fas fa-star yellow-text"></i>
                        <?php endfor; ?>
                        <h6><strong>Marca: </strong> <?= $categoria->nome ?></h6>
                        <p>Por: <span class="h4"><strong>R$ <?= number_format($produto->preco/1.10, 2, ',', '.') ?></strong></span> à vista com desconto no Boleto.</p>
                        <p class="h7">Ou 12 vezes de R$ <?= number_format($produto->preco/12, 2, ',', '.') ?> Sem Juros.</p>
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link" id="parcelamento-tab" data-toggle="tab" href="#parcelamento" role="tab" aria-controls="parcelamento" aria-selected="true">Parcelamento</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active" id="frete-tab" data-toggle="tab" href="#frete" role="tab" aria-controls="frete" aria-selected="false">Frete</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade" id="parcelamento" role="tabpanel" aria-labelledby="parcelamento-tab">
                                <table class="table table-borderless">
                                    <tbody>
                                        <?php for($i=1;$i<=12;$i++): ?>
                                        <tr>
                                            <th><?= $i ?>x</th>
                                            <td><?= number_format($produto->preco/$i, 2, ',', '.') ?></td>
                                            <td>Sem Juros</td>
                                        </tr>
                                        <?php endfor; ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane fade show active" id="frete" role="tabpanel" aria-labelledby="frete-tab">
                                <table class="table table-borderless">
                                    <thead>
                                        <tr>
                                            <th scope="col">Entrega</th>
                                            <th scope="col">Frete</th>
                                            <th scope="col">Prazo</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th>Rapida</th>
                                            <td>Grátis</td>
                                            <td>4 dias úteis</td>
                                        </tr>
                                        <tr>
                                            <th>Convencional</th>
                                            <td>14,99</td>
                                            <td>5 dias úteis</td>
                                        </tr>
                                        <tr>
                                            <th>Agendada</th>
                                            <td>14,99</td>
                                            <td>A agendar</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <h3>Descrição</h3>
                    <p class="text-justify"><?= $produto->descricao ?></p>
                </div>
            </div>
        </div>
    </div>
</div>
