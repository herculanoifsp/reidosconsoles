<div class="clearfix mt-5 mb-5">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <h4 class="section-heading h2 mb-5"><?= $marca." / Lista de ".$categoria ?></h4>
                <hr class="my-5">
                <br/>
                <div class="row">
                <?= $cards ?>
                </div>
            </div>
        </div>
    </div>
</div>