<div class="clearfix mt-5 mb-5">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-12">
                <h4 class="section-heading h2 mb-5">Destaque</h4>
                <hr class="my-5">
                <ul class="list-group">
                    <?php foreach($situacao as $item): ?>
                        <?php if($item->situacao == "dest"): ?>
                        <li class="list-group-item">
                            <a class="blue-text" href=""><i class="fas fa-edit mr-4 pr-3"></i></a> <?= $item->nome ?>
                        </li>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </ul>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <h4 class="section-heading h2 mb-5">Promoção</h4>
                <hr class="my-5">
                <ul class="list-group">
                    <?php foreach($situacao as $item): ?>
                        <?php if($item->situacao == "promo"): ?>
                        <li class="list-group-item">
                            <a class="blue-text" href=""><i class="fas fa-edit mr-4 pr-3"></i></a> <?= $item->nome ?>
                        </li>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </ul>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <h4 class="section-heading h2 mb-5">Lançamento</h4>
                <hr class="my-5">
                <ul class="list-group">
                    <?php foreach($situacao as $item): ?>
                        <?php if($item->situacao == "lanc"): ?>
                        <li class="list-group-item">
                            <a class="blue-text" href=""><i class="fas fa-edit mr-4 pr-3"></i></a> <?= $item->nome ?>
                        </li>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
</div>