<div class="container mt-5 mb-5">
    <div class="row mt-5 mb-5">
        <div class="col-md-12 col-sm-12 col-xs-12 mt-5 mb-5">
            <form class="text-center border border-light p-5" method='post' action="#">
                <p class="h4 mb-4">Cadastro de Produtos</p>
                <?php if(!empty($error)): ?>

                <div class="alert alert-info" role="alert">
                    <?= $error->error ?>
                </div>

                <?php endif; ?>
                <?= validation_errors() ?>
                <input type="text" id="nome_produto" name="nome" value="<?= isset($produto->nome) ? $produto->nome : set_value('nome') ?>" class="form-control mb-4" placeholder="Nome do Produto">

                <div class="form-group">
                    <textarea class="form-control rounded-0" id="descricao" name="descricao" rows="3" placeholder="Descrição do Produto"><?= isset($produto->descricao) ? $produto->descricao : set_value('descricao') ?></textarea>
                </div>
                
                <input type="text" id="preco" name="preco" class="form-control mb-4" value="<?= isset($produto->preco) ? $produto->preco : set_value('preco') ?>" placeholder="Preco">

                <label>Estrelas</label>
                <select class="browser-default custom-select mb-4" name="estrelas" id="estrelas" required>
                    <option value="" disabled selected>Escolha uma opção</option>
                    <option value="1" <?= set_value('estrelas') == "1" ? "selected" : "" ?> <?= isset($produto->estrela) && $produto->estrela == 1 ? "selected" : "" ?>>1ª Estrela</option>
                    <option value="2" <?= set_value('estrelas') == "2" ? "selected" : "" ?> <?= isset($produto->estrela) && $produto->estrela == 2 ? "selected" : "" ?>>2ª Estrela</option>
                    <option value="3" <?= set_value('estrelas') == "3" ? "selected" : "" ?> <?= isset($produto->estrela) && $produto->estrela == 3 ? "selected" : "" ?>>3ª Estrela</option>
                    <option value="4" <?= set_value('estrelas') == "4" ? "selected" : "" ?> <?= isset($produto->estrela) && $produto->estrela == 4 ? "selected" : "" ?>>4ª Estrela</option>
                    <option value="5" <?= set_value('estrelas') == "5" ? "selected" : "" ?> <?= isset($produto->estrela) && $produto->estrela == 5 ? "selected" : "" ?>>5ª Estrela</option>
                </select>
                
                <label>Marca e Categoria do Porduto</label>
                <select class="browser-default custom-select mb-4" name="categoria" id="categoria" required>
                    <option value="" disabled selected>Escolha uma opção</option>
                    <?php foreach($categorias as $item): ?>
                        <optgroup label="<?= $item->nome ?>">
                            <?php foreach($item->filhos as $cat): ?>
                            <option value="<?= $cat->id ?>" <?= set_value('categoria') == $cat->id ? "selected" : "" ?> <?= isset($produto->id_categoria) && $produto->id_categoria->id == $cat->id ? "selected" : "" ?> ><?= $cat->nome ?></option>
                            <?php endforeach; ?>
                        </optgroup>
                    <?php endforeach; ?>
                </select>
                
                <div class="row mb-4">
                    <div class="col-sm-4 col-xs-12">
                        <div class="custom-control custom-switch">
                            <input type="checkbox" class="custom-control-input" name="destaque" <?= set_value('destaque') == "on" ? "checked" : ""?> <?= isset($produto->situacao) && isset($produto->situacao[0]->categoria) ? "checked" : "" ?> id="situacao1">
                            <label class="custom-control-label" for="situacao1">Destaque</label>
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <div class="custom-control custom-switch">
                            <input type="checkbox" class="custom-control-input" name="promocao" <?= set_value('promocao') == "on" ? "checked" : ""?> <?= isset($produto->situacao) && isset($produto->situacao[1]->categoria) ? "checked" : "" ?> id="situacao2">
                            <label class="custom-control-label" for="situacao2">Promoção</label>
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <div class="custom-control custom-switch">
                            <input type="checkbox" class="custom-control-input" name="lancamento" <?= set_value('lancamento') == "on" ? "checked" : ""?> <?= isset($produto->situacao) && isset($produto->situacao[2]->categoria) ? "checked" : "" ?> id="situacao3">
                            <label class="custom-control-label" for="situacao3">Lançamento</label>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="id_produto" value="<?= isset($produto->id) ? $produto->id : ""  ?>" />
                <button class="btn btn-outline-primary waves-effect btn-block" type="submit">Salvar</button>
            </form>
        </div>
        <a href="<?= base_url("Admin/index")?>" class="btn btn-outline-danger waves-effect">Voltar</a>
    </div>
</div>
