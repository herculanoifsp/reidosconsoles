<div class="clearfix mt-5 mb-5">
    <div class="container">
        <?php if($erro): ?>
            <div class="alert alert-danger" role="alert">
                <?= $erro ?>
            </div>
        <?php endif; ?>
        <h4 class="section-heading h2 mb-5">Lista de Produtos</h4>
        <hr class="my-5">
        <div class="row">
            <div class="col-sm-3">
                <a href="<?= base_url("Admin/form_cadastro_produto") ?>" class="btn btn-outline-info waves-effect">Inserir Produto</a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="table-responsive">
                    <table id="dtProduto" class="table table-bordered" cellspacing="0" width="100%">
                        <thead class="<?= $color->color_site ?> white-text">
                            <tr>
                                <th class="th-sm">Nome</th>
                                <th class="th-sm">Preço</th>
                                <th class="th-sm">Categoria</th>
                                <th class="th-sm">Destaque</th>
                                <th class="th-sm">Promoção</th>
                                <th class="th-sm">Lançamento</th>
                                <th class="th-sm">Edita</th>
                                <th class="th-sm">Excluir</th>
                                <th class="th-sm">Ver Imagens</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($produto_table as $key => $item):  ?>
                            <tr>
                                <td><?= $item->nome ?></td>
                                <td>R$ <?= $item->preco ?></td>
                                <td><?= $item->id_categoria->nome." - ".$item->id_categoria->pai->nome ?></td>
                                <?php if(isset($item->situacao[0]->categoria)): ?>
                                <td class="text-center"><i class="fas fa-check text-secondary"></i></td>
                                <?php else: ?>
                                <td class="text-center"><i class="fas fa-minus text-secondary"></i></td>
                                <?php endif; ?>
                                <?php if(isset($item->situacao[1]->categoria)): ?>
                                <td class="text-center"><i class="fas fa-check text-success"></i></td>
                                <?php else: ?>
                                <td class="text-center"><i class="fas fa-minus text-success"></i></td>
                                <?php endif; ?>
                                <?php if(isset($item->situacao[2]->categoria)): ?>
                                <td class="text-center"><i class="fas fa-check text-primary"></i></td>
                                <?php else: ?>
                                <td class="text-center"><i class="fas fa-minus text-primary"></i></td>
                                <?php endif; ?>
                                <td><a href="<?= base_url("Admin/form_edita_produto/$item->id") ?>" class="btn btn-outline-warning waves-effect"><i class="fas fa-edit"></i></a></td>
                                <td><a href="<?= base_url("Admin/excluir_produto/$item->id") ?>" class="btn btn-outline-danger waves-effect"><i class="fas fa-trash"></i></a></td>
                                <td><a href="<?= base_url("Admin/lista_imagem/$item->id") ?>" class="btn btn-outline-info waves-effect"><i class="fas fa-search"></i></a></td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th >Nome</th>
                                <th >Preço</th>
                                <th >Categoria</th>
                                <th >Destaque</th>
                                <th >Promoção</th>
                                <th >Lançamento</th>
                                <th >Edita</th>
                                <th >Excluir</th>
                                <th>Ver Imagens</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
        <hr class="my-5 pink accent-3">
    </div>
</div>