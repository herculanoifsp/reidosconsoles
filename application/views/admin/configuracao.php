<div class="clearfix mt-5 mb-5">
    <div class="container">
    	<h4 class="section-heading h2">Configurações Basicas da Pagina</h4>
    	<div class="row">
    		<div class="col-md-4 col-sm-4 col-xs-12 text-center">
    			<hr class="my-5">
    			<a href="<?= base_url("Admin/form_contato") ?>" class="btn btn-outline-info waves-effect">Informações do Contato</a>
    		</div>
    		<div class="col-md-4 col-sm-4 col-xs-12 text-center">
    			<hr class="my-5">
    			<a href="<?= base_url("Admin/form_carrousel") ?>" class="btn btn-outline-info waves-effect">Imagens do Carrousel(index)</a>
    		</div>
    		<div class="col-md-4 col-sm-4 col-xs-12 text-center">
    			<hr class="my-5">
                <button class="btn btn-outline-info dropdown-toggle mr-4" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Editar Imagens dos Itens Menu</button>
                <div class="dropdown-menu">
                    <?php foreach ($lista_menu as $item): ?>
                    <a class="dropdown-item" href="<?= base_url("Admin/edita_img_item/$item->id") ?>"><?= $item->nome ?></a>
                    <?php endforeach; ?>
                </div>
    		</div>
    	</div>
    	<hr class="my-5 pink accent-3">
    </div>
</div>
