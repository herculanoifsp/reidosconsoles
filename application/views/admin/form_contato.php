<div class="container mt-5 mb-5">
    <div class="row mt-5 mb-5">
        <div class="col-md-12 col-sm-12 col-xs-12 mt-5 mb-5">
            <form class="text-center border border-light p-5" method='post' action="#">
                <p class="h4 mb-4">Atualização de Informações de Contato(Rodapé)</p>
                <?php if(!empty($error)): ?>

                <div class="alert alert-info" role="alert">
                    <?= $error->error ?>
                </div>

                <?php endif; ?>
                <?= validation_errors() ?>
                <input type="text" id="sobre" name="sobre" class="form-control mb-4" value="<?= $contato->sobre ?>" placeholder="Sobre a Loja">

                <input type="text" id="horario" name="horario" class="form-control mb-4" value="<?= $contato->horario ?>" placeholder="Horario de Funcionamento">

                <input type="text" id="telefone" name="telefone" class="form-control mb-4" value="<?= $contato->telefone ?>" placeholder="Telefone Fixo">

                <input type="text" id="email" name="email" class="form-control mb-4" value="<?= $contato->email ?>" placeholder="E-mail">

                <input type="text" id="celular" name="celular" class="form-control mb-4" value="<?= $contato->celular ?>" placeholder="Telefone Celular">

                <input type="text" id="facebook" name="facebook" class="form-control mb-4" value="<?= $contato->facebook ?>" placeholder="Facebook">

                <input type="text" id="twitter" name="twitter" class="form-control mb-4" value="<?= $contato->twitter ?>" placeholder="Twitter">

                <input type="text" id="instagram" name="instagram" class="form-control mb-4" value="<?= $contato->instagram ?>" placeholder="Instagram">

                <select class="browser-default custom-select mb-4" name="color" id="color" required>
                    <?php foreach($colores as $item): ?>
                    <option value="<?= $item->nome ?>" <?= $contato->color_site == $item->nome ? "selected" : "" ?>><?= $item->nome ?></option>
                    <?php endforeach; ?>
                </select>

                <button class="btn btn-outline-primary waves-effect btn-block" type="submit">Salvar</button>
            </form>
        </div>
        <a href="<?= base_url("Admin/index")?>" class="btn btn-outline-danger waves-effect">Voltar</a>
    </div>
</div>

