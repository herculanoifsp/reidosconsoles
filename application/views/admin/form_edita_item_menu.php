<div class="container mt-5 mb-5">
    <div class="row mt-5 mb-5">
        <div class="col-md-12 col-sm-12 col-xs-12 mt-5 mb-5">
            <form class="text-center border border-light p-5" method='post' action="#" enctype="multipart/form-data">
                <p class="h4 mb-4">Atualização de Imagem dos Item Principal do menu</p>
                <?php if(!empty($error)): ?>

                <div class="alert alert-info" role="alert">
                    <?= $error->error ?>
                </div>

                <?php endif; ?>
                <?= validation_errors() ?>
                <div class="row mb-4">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <img src="<?= base_url("assets/img/$cat_u->img") ?>" alt="thumbnail" class="img-thumbnail">
                        <div class="text-center">Imagem de Fundo</div>
                    </div>
                </div>

                <input type="text" id="nome" name="nome" class="form-control mb-4" value="<?= $cat_u->nome ?>" placeholder="Nome do item">

                <div class="input-group mb-4">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="Imagem_cat_addon">Imagem</span>
                    </div>
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="Imagem_cat" name="img" aria-describedby="Imagem_cat_addon">
                        <label class="custom-file-label" for="Imagem_cat">Escolha um Arquivo</label>
                    </div>
                </div>
                <button class="btn btn-outline-primary waves-effect btn-block" type="submit">Salvar</button>
            </form>
        </div>
        <a href="<?= base_url("Admin/index")?>" class="btn btn-outline-danger waves-effect">Voltar</a>
    </div>
</div>