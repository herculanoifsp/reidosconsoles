<div class="container mt-5 mb-5">
    <div class="row mt-5 mb-5">
        <div class="col-md-12 col-sm-12 col-xs-12 mt-5 mb-5">
            <form class="text-center border border-light p-5" method='post' action="#" enctype="multipart/form-data">
            	<p class="h4 mb-4">Cadastro de Item de Menu</p>
            	<?php if(!empty($error)): ?>

                <div class="alert alert-info" role="alert">
                    <?= $error->error ?>
                </div>

                <?php endif; ?>
                <?= validation_errors() ?>

                <label>Marca Pai</label>
                <select class="browser-default custom-select mb-4" name="marca" id="marca" required>
                    <option value="" disabled selected>Escolha uma opção</option>
                    <option value="0">Nova Marca</option>
                    <?php foreach($categorias as $item): ?>
                        <option value="<?= $item->id ?>" <?= isset($cat_u->pai) && $cat_u->pai->id == $item->id ? "selected" : "" ?> ><?= $item->nome ?></option>
                    <?php endforeach; ?>
                </select>

                <label>Nome do Item</label>
                <input type="text" id="nome" name="nome" class="form-control mb-4" value="<?= isset($cat_u->nome) ? $cat_u->nome : set_value('nome') ?>" placeholder="Nome do Item">

                <div class="input-group mb-4">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="Imagem_cat_addon">Imagem</span>
                    </div>
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="Imagem_cat" name="img" aria-describedby="Imagem_cat_addon">
                        <label class="custom-file-label" for="Imagem_cat">Escolha um Arquivo</label>
                    </div>
                </div>
                <small id="Imagem_cat" class="form-text text-muted mb-4">
                    Caso seja uma marca nova.
                </small>

                <button class="btn btn-outline-primary waves-effect btn-block" type="submit">Salvar</button>
            </form>
        </div>
        <a href="<?= base_url("Admin/index")?>" class="btn btn-outline-danger waves-effect">Voltar</a>
    </div>
</div>