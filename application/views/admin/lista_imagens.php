<div class="clearfix mt-5 mb-5">
    <div class="container">
    	<?php if($erro): ?>
            <div class="alert alert-danger" role="alert">
                Ocorreu um erro ao excluir o arquivo, tente novamente mais tarde
            </div>
        <?php endif; ?>
    	<h4 class="section-heading h2 mb-5">Imagens</h4>
        <hr class="my-5">
        <div class="row mb-3">
            <div class="col-sm-3">
                <a href="<?= base_url("Admin/form_inseri_imagem/$id") ?>" class="btn btn-outline-info waves-effect">Inserir Imagem</a>
            </div>
        </div>
        <div class="row">
        	<?php foreach($lista as $item): ?>
                <?php $ext = explode(".", $item->img) ?>
        	<div class="col-md-4 col-sm-4 col-xs-12">
				<img src="<?= base_url("assets/img/$item->img") ?>" alt="thumbnail" class="img-thumbnail">
				<div class="text-center">
					<a href="<?= base_url("Admin/form_edita_imagem/$id/$item->id") ?>" class="btn btn-outline-warning waves-effect"><i class="fas fa-edit"></i></a>
					<a href="<?= base_url("Admin/delete_imagem_prod/$item->id/$id") ?>" class="btn btn-outline-danger waves-effect"><i class="fas fa-trash"></i></a>
				</div>
			</div>	
    		<?php endforeach; ?>
        </div>
        <a href="<?= base_url("Admin/index")?>" class="btn btn-outline-danger waves-effect mt-5">Voltar</a>
    </div>
</div>