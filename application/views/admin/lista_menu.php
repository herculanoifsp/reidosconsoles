<div class="clearfix mt-5 mb-5">
    <div class="container">
        <div class="row">
            <a href="<?= base_url("Admin/form_item_menu") ?>" class="btn btn-outline-info waves-effect mb-5">Inserir Novo Item</a>
        </div>
        <h4 class="section-heading h2">Itens do Menu</h4>
        <hr class="my-5">
        <div class="row">
            <?php foreach($lista_menu as $item): ?>
            <div class="col-md-4 col-sm-6 col-xs-12 mb-5">
                <h4 class="section-heading h2 mb-5"><?= $item->nome ?><a class="blue-text text-right float-right" href="<?= base_url("Admin/deleta_item_menu/$item->id") ?>"><i class="fas fa-trash red-text fa-xs"></i></a></h4>
                <hr class="my-5">
                <ul class="list-group">
                    <?php foreach($item->filho as $filho): ?>
                    <li class="list-group-item">
                        <div class="md-v-line"></div><a class="blue-text" href="<?= base_url("Admin/form_edita_item_menu/$filho->id") ?>"><i class="fas fa-edit text-warning mr-4 pr-3"></i></a><div class="md-v-line_r"></div><a class="blue-text" href="<?= base_url("Admin/deleta_item_menu/$filho->id") ?>"><i class="fas fa-trash red-text mr-4 pr-3"></i></a> <?= $filho->nome ?>
                    </li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <?php endforeach; ?>
        </div>
        <hr class="my-5 pink accent-3">
    </div>
</div>