<div class="container mt-5 mb-5">
    <div class="row mt-5 mb-5">
        <div class="col-md-12 col-sm-12 col-xs-12 mt-5 mb-5">
            <form class="text-center border border-light p-5" method='post' action="#" enctype="multipart/form-data">
                <p class="h4 mb-4">Atualização de Imagem do Carrousel</p>
                <?php if(!empty($error)): ?>

                <div class="alert alert-info" role="alert">
                    <?= $error->error ?>
                </div>

                <?php endif; ?>
                <?= validation_errors() ?>
                <div class="row">
                    <?php foreach($carrossel as $key => $item): ?>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <img src="<?= base_url("assets/img/$item->img") ?>" alt="thumbnail" class="img-thumbnail">
                        <div class="text-center">
                            <?= $key+1 ?>º Slide
                            <div class="float-right"><a href="<?= base_url("Admin/delete_imagem_carrossel/$item->id") ?>"><i class="fas fa-trash red-text"></i></a></div>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>

                <label class="mt-4">Slide</label>
                <select class="browser-default custom-select mb-4" name="slide" id="slide" required>
                    <option value="" disabled selected>Escolha uma opção</option>
                    <option value="0">Novo Slide</option>
                    <?php foreach($carrossel as $key => $item): ?>
                    <option value="<?= $item->id ?>"><?= $key+1 ?>º Slide</option>
                    <?php endforeach; ?>
                </select>

                <div class="input-group mb-4">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="Imagem_cat_addon">Imagem</span>
                    </div>
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="Imagem_cat" name="img" aria-describedby="Imagem_cat_addon">
                        <label class="custom-file-label" for="Imagem_cat">Escolha um Arquivo</label>
                    </div>
                </div>

                <button class="btn btn-outline-primary waves-effect btn-block" type="submit">Salvar</button>
            </form>
        </div>
        <a href="<?= base_url("Admin/index")?>" class="btn btn-outline-danger waves-effect">Voltar</a>
    </div>
</div>