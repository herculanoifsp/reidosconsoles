<div class="clearfix mt-5 mb-5">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div id="carouselExampleControls" class="carousel slide" data-ride="carousel" data-interval="2500">
                    <div class="carousel-inner">
                      <?php foreach($carrossel as $key => $item): ?>
                        <div class="carousel-item <?= $key == 0 ? "active" : "" ?>">
                            <img class="d-block w-100 img_carrousel_a" src="<?= base_url("assets/img/$item->img") ?>" alt="<?= $key+1 ?>º slide">
                        </div>
                        <?php endforeach; ?>
                    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                      <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                      <span class="carousel-control-next-icon" aria-hidden="true"></span>
                      <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>