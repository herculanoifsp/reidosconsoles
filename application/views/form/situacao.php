<div class="container mt-5 mb-5">
    <div class="row mt-5 mb-5">
        <div class="col-md-12 col-sm-12 col-xs-12 mt-5 mb-5">
            <form class="text-center border border-light p-5" method="post" action="#">
                <p class="h4 mb-4">Formulário de Situação do Produto</p>
                <select class="browser-default custom-select mb-4">
                    <option selected disabled>Produto</option>
                    <?php foreach($produto as $item): ?>
                    <option value="<?= $item->id ?>"><?= $item->nome ?></option>
                    <?php endforeach; ?>
                </select>
                <select class="browser-default custom-select mb-4">
                    <option selected disabled>Situação</option>
                    <option value="lanc">Lançamento</option>
                    <option value="dest">Destaque</option>
                    <option value="promo">Promoção</option>
                </select>
                <button class="btn btn-info my-4 btn-block" type="submit">Sign in</button>
                <hr>
            </form>
        </div>
    </div>
</div>
    
<!--<div class="md-form">
    <i class="fas fa-user prefix"></i>
    <input type="text" id="inputIconEx2" class="form-control">
    <label for="inputIconEx2">E-mail address</label>
</div>-->