<div class="container mt-5 mb-5">
    <div class="row mt-5 mb-5">
        <div class="col-md-12 col-sm-12 col-xs-12 mt-5 mb-5">
            <form class="text-center border border-light p-5" method='post' action="#">
            <?php if(!empty($error)): ?>

            <div class="alert alert-info" role="alert">
                <?= $error->error ?>
            </div>

            <?php endif; ?>
            <p class="h4 mb-4">Formulário de Login</p>
            <?= validation_errors() ?>
            <input type="text" id="login" name="login" class="form-control mb-4" placeholder="Email ou Apelido">
            <input type="password" id="senha" name="senha" class="form-control mb-4" placeholder="Senha">
            <button class="btn btn-info btn-block my-4" type="submit">Entrar</button>
        </form>
        </div>
    </div>
</div>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>