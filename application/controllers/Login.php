<?php

class Login extends MY_Controller{
    
    function __construct()
    {
        parent:: __construct();
        $this->load->model("Login_Model", "m_login");
        $this->load->model("Home_Model", "m_home");
        $this->data["navbar"] = $this->m_home->get_item_navbar();
        $this->data["rodape"] = $this->m_home->get_item_rodape();
    }
    
    public function index()
    {
        $this->form_login();
    }

    public function form_login()
    {
        $data['error'] = $this->m_login->acesso_login();
        if(isset($data['error']) && $data['error']->result == true)
        {
            redirect(base_url("Admin/"));
        }
        $this->conteudo["login"] = $this->load->view("login/form_login", $data, true);
        
        $this->show($this->conteudo, $this->data);
    }
    
}
