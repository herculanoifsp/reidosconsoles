<?php

class Admin extends MY_Controller{
    
    function __construct()
    {
        parent:: __construct();
        $this->load->model("Admin_Model", "m_admin");
        $this->load->model("Home_Model", "m_home");
        $this->data["navbar"] = $this->m_admin->get_categoria();
        $this->data["rodape"] = $this->m_home->get_item_rodape();
    }
    
    public function index($erro = null)
    {
        $data["produto_table"] = $this->m_admin->get_produto_table();
        $data["lista_menu"] = $this->m_admin->get_itens_menu();
        $data["color"] = $this->m_home->get_item_rodape();
        $data["erro"] = $erro;
        $this->conteudo["tabela"] = $this->load->view("admin/table_produto", $data, true);
        $this->conteudo["configuracao"] = $this->load->view("admin/configuracao", null, true);
        $this->conteudo["lista_menu"] = $this->load->view("admin/lista_menu", $data, true);
        
        $this->show($this->conteudo, $this->data);
    }
    
    /** CRUD Produto **/

    public function form_cadastro_produto()
    {
        $data['error'] = $this->m_admin->cadastro_produto();
        if(isset($data['error']) && $data['error']->result == true)
            redirect(base_url("Admin/lista_imagem/".$data['error']->id));
        $data['categorias'] = $this->m_admin->get_categoria();
        $this->conteudo["cadastro"] = $this->load->view("admin/form_produto", $data, true);
        
        $this->show($this->conteudo, $this->data);
    }
    
    public function form_edita_produto($id)
    {
        $data['error'] = $this->m_admin->cadastro_produto($id);
        if(isset($data['error']) && $data['error']->result == true)
            redirect(base_url("Admin"));
        
        $data['categorias'] = $this->m_admin->get_categoria();
        $data['produto'] = $this->m_admin->get_produto_id($id);
        $this->conteudo["edita"] = $this->load->view("admin/form_produto", $data, true);
        
        $this->show($this->conteudo, $this->data);
    }
    
    public function excluir_produto($id)
    {
        $rs = $this->m_admin->exclui_produto($id);
        if($rs->result)
        {
            redirect(base_url("Admin"));
        }
        else
        {
            redirect(base_url("Admin/$rs->error"));
        }
        
    }

    /** Fim CRUD Produto **/


    /** CRUD imagem **/
    public function lista_imagem($id, $error = null)
    {
        $data["id"] = $id;
        $data["erro"] = $error;
        $data["lista"] = $this->m_admin->get_imagens($id);
        $this->conteudo["lista"] = $this->load->view("admin/lista_imagens", $data, true);

        $this->show($this->conteudo, $this->data);
    }

    public function form_inseri_imagem($id)
    {
        $data['error'] = $this->m_admin->cadastro_imagens($id);
        if(isset($data['error']) && $data['error']->result == true)
            redirect(base_url("Admin/lista_imagem/$id"));
        
        $data["produto"] = $this->m_admin->get_produto_id($id);
        $data["id"] = $id;
        $data["id_img"] = ($this->m_admin->get_ultimo_id()) + 1;
        $data["edit"] = 0;
        $this->conteudo["cadastro"] = $this->load->view("admin/form_imagem", $data, true);
        
        $this->show($this->conteudo, $this->data);
    }

    public function form_edita_imagem($id, $id_img)
    {
        $data['error'] = $this->m_admin->cadastro_imagens($id);
        if(isset($data['error']) && $data['error']->result == true)
            redirect(base_url("Admin/lista_imagem/$id"));
        
        $data["produto"] = $this->m_admin->get_produto_id($id);
        $data["id"] = $id;
        $data["id_img"] = $id_img;
        $data["edit"] = 1;
        $this->conteudo["cadastro"] = $this->load->view("admin/form_imagem", $data, true);
        
        $this->show($this->conteudo, $this->data);
    }

    public function delete_imagem_prod($id, $id_prod)
    {
        $rs = $this->m_admin->exclui_imagem_prod($id);
        if($rs->result)
        {
            redirect(base_url("Admin/lista_imagem/$id_prod"));
        }
        else
        {
            redirect(base_url("Admin/lista_imagem/$id_prod/1"));
        }
    }

    /** Fim CRUD Imagem **/
    

    /** CRUD Item menu **/
    public function form_item_menu()
    {
        $data['error'] = $this->m_admin->cadastro_item_menu();
        if(isset($data['error']) && $data['error']->result == true)
            redirect(base_url("Admin"));
        $data['categorias'] = $this->m_admin->get_itens_menu();        
        $this->conteudo["cadastro"] = $this->load->view("admin/form_item_menu", $data, true);

        
        $this->show($this->conteudo, $this->data);
    }

    public function form_edita_item_menu($id)
    {
        $data['error'] = $this->m_admin->cadastro_item_menu($id);
        if(isset($data['error']) && $data['error']->result == true)
            redirect(base_url("Admin"));
        $data['categorias'] = $this->m_admin->get_itens_menu();
        $data['cat_u'] = $this->m_admin->get_item_menu($id);

        $this->conteudo["edicao"] = $this->load->view("admin/form_item_menu", $data, true);
        
        
        $this->show($this->conteudo, $this->data);
    }

    public function deleta_item_menu($id)
    {
        $rs = $this->m_admin->exclui_item_menu($id);
        if($rs->result)
        {
            redirect(base_url("Admin"));
        }
        else
        {
            redirect(base_url("Admin/$rs->error"));
        }
    }

    public function edita_img_item($id)
    {
        $data['error'] = $this->m_admin->atualiza_img_menu($id);
        if(isset($data['error']) && $data['error']->result == true)
            redirect(base_url("Admin"));
        $data['cat_u'] = $this->m_admin->get_item_menu($id);
        $this->conteudo["cadastro"] = $this->load->view("admin/form_edita_item_menu", $data, true);

        
        $this->show($this->conteudo, $this->data);
    }

    /** Fim CRUD Item Menu **/
    
    /** Contato **/
    public function form_contato()
    {
        $data['error'] = $this->m_admin->cadastro_contato();
        if(isset($data['error']) && $data['error']->result == true)
            redirect(base_url("Admin"));
        
        $data['contato'] = $this->m_home->get_item_rodape();
        $data['colores'] = $this->m_admin->get_colores();
        $this->conteudo["contato"] = $this->load->view("admin/form_contato", $data, true);
        
        $this->show($this->conteudo, $this->data);
    }
    /** Fim Contato **/

    /** Formulário Carrosel */
    public function form_carrousel()
    {
        $data['error'] = $this->m_admin->atualiza_carrossel();
        if(isset($data['error']) && $data['error']->result == true)
            redirect(base_url("Admin/form_carrousel"));
        
        $data['carrossel'] = $this->m_admin->get_img_carrousel();
        $this->conteudo["carrosel"] = $this->load->view("admin/form_carrousel", $data, true);
        
        $this->show($this->conteudo, $this->data);
    }

    public function delete_imagem_carrossel($id)
    {
        $rs = $this->m_admin->exclui_imagem_carrousel($id);
        if($rs->result)
        {
            redirect(base_url("Admin/form_carrousel"));
        }
        else
        {
            redirect(base_url("Admin/form_carrousel/$rs->error"));
        }
    }
    /** Fim Formulário Carrosel */
    
}
