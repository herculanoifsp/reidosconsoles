<?php

class Home extends MY_Controller{
    
    function __construct()
    {
        parent:: __construct();
        $this->load->model("Home_Model", "m_home");
        $this->load->model("Admin_Model", "m_admin");
        $this->data["navbar"] = $this->m_home->get_item_navbar();
        $this->data["rodape"] = $this->m_home->get_item_rodape();
    }
    
    public function index()
    {
        $data["destaque"] = $this->m_home->get_destaques_promocao("dest");
        $data["lancamento"] = $this->m_home->get_destaques_promocao("lanc");
        $data["promocao"] = $this->m_home->get_destaques_promocao("promo");
        $data["carrossel"] = $this->m_admin->get_img_carrousel();
        $data["card_destaque"] = $this->m_home->get_card($data["destaque"]);
        $data["card_lancamento"] = $this->m_home->get_card($data["lancamento"]);
        $data["card_promocao"] = $this->m_home->get_card($data["promocao"]);
        
        $this->conteudo["carousel"] = $this->load->view("componentes/carousel", $data, true);
        $this->conteudo["card"] = $this->load->view("home/card_situacao", $data, true);
        
        $this->show($this->conteudo, $this->data);
    }
    
    public function lista_produtos($marca, $categoria)
    {
        $data["lista"] = $this->m_home->get_lista($marca, $categoria);
        $data["categoria"] = $categoria;
        $data["marca"] = $marca;
        $data["cards"] = $this->m_home->get_card($data["lista"]);
        $this->conteudo["card"] = $this->load->view("home/card_lista", $data, true);
        
        $this->data["navbar_d"] = $this->m_home->get_categoria_nome($marca);
        $this->show2($this->conteudo, $this->data);
    }
    
    public function detalhes($id)
    {
        $data["produto"] = $this->m_home->get_produto_id($id);
        $data["imagem"] = $this->m_home->get_imagem_id($id);
        $data["categoria"] = $this->m_home->get_categoria_id($data["produto"]->id_categoria);
        
        $this->conteudo["pagina"] = $this->load->view("home/detalhes", $data, true);
        
        $this->show($this->conteudo, $this->data);
    }
    
}