<?php

defined('BASEPATH') or exit('No direct script access allowe');

class Validation extends CI_Object{
        
    public function login_validation()
    {
        $this->form_validation->set_rules("login", 'Login', 'trim|required|max_length[40]|min_length[3]');
        $this->form_validation->set_rules("senha", 'Senha', 'trim|required|max_length[40]|min_length[7]');

        return $this->form_validation->run();
    }

    public function cadastro_produto_validation()
    {
        $this->form_validation->set_rules("nome", 'Nome do Produto', 'trim|required|max_length[100]|min_length[1]');
        $this->form_validation->set_rules("descricao", 'Descrição do Produto', 'trim|required|max_length[2000]|min_length[10]');
        $this->form_validation->set_rules("preco", 'Preço do Produto', 'trim|required|decimal');

        return $this->form_validation->run();
    }

    public function cadastro_item_menu_validation()
    {
        $this->form_validation->set_rules("nome", 'Nome do Item', 'trim|required|max_length[25]|min_length[1]');

        return $this->form_validation->run();   
    }

    public function cadastro_contato_validation()
    {
        $this->form_validation->set_rules("sobre", 'Sobre a Empresa', 'trim|required|max_length[100]|min_length[1]');
        $this->form_validation->set_rules("horario", 'Horario', 'trim|required|max_length[100]|min_length[1]');
        $this->form_validation->set_rules("telefone", 'Telefone Fixo', 'trim|required|max_length[20]|min_length[1]');
        $this->form_validation->set_rules("email", 'Email', 'trim|required|max_length[70]|min_length[1]');
        $this->form_validation->set_rules("celular", 'Telefone Celular', 'trim|required|max_length[20]|min_length[1]');
        $this->form_validation->set_rules("facebook", 'Facebook', 'trim|required|max_length[100]|min_length[1]');
        $this->form_validation->set_rules("twitter", 'Twitter', 'trim|required|max_length[100]|min_length[1]');
        $this->form_validation->set_rules("instagram", 'Instagram', 'trim|required|max_length[100]|min_length[1]');

        return $this->form_validation->run();
    }

    public function atualiza_img_menu_validation()
    {
        $this->form_validation->set_rules("nome", 'Nome da Categoria', 'trim|required|max_length[100]|min_length[1]');

        return $this->form_validation->run();
    }
}