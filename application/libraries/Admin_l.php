<?php

defined('BASEPATH') OR exit('No direct script');

class Admin_l extends CI_Object{
    
    /** Produto */
    public function salva_produto($data)
    {
        $rst = (object)array("result" => false, "error" => "");
        
        $this->db->set("nome", $data->nome);
        $this->db->set("descricao", $data->descricao);
        $this->db->set("preco", $data->preco);
        $this->db->set("estrela", $data->estrelas);
        $this->db->set("id_categoria", $data->categoria);
        $this->db->set("data_inclusao", "NOW()", false);
        
        if($data->id != null)
        {
            $this->db->where("id", $data->id);
            if(!$this->db->update("produto"))
            {
                $rst->error = "Erro ao atualizar os dados. Por favor tente novamente mais tarde";
                return $rst;
            }
        }
        else
        {
            if(!$this->db->insert("produto"))
            {
                $rst->error = "Erro ao inserir os dados. Por favor tente novamente mais tarde";
                return $rst;
            }
            else
            {
                $id = $this->db->insert_id();
                $data->id = $id;
                $rst->id = $id;
            }
        }
        
        $this->salva_situacao($data);
            
        $rst->result = true;
        
        return $rst;
    }
    
    public function salva_situacao($data)
    {
        $query = $this->get_destaque_promocao($data->id);
        
        if(!isset($query[0]->categoria))
        {
            if(isset($data->destaque))
            {
                $this->db->set("id_produto", $data->id);
                $this->db->set("categoria", "dest");

                $this->db->insert("destaque_promocao");
            }
        }
        elseif(!isset($data->destaque))
        {
            $this->db->where("id_produto = '$data->id' AND categoria = 'dest'");
            $this->db->delete("destaque_promocao");
        }
        
        if(!isset($query[1]->categoria))
        {
            if(isset($data->promocao))
            {
                $this->db->set("id_produto", $data->id);
                $this->db->set("categoria", "promo");

                $this->db->insert("destaque_promocao");
            }
        }
        elseif(!isset($data->promocao))
        {
            $this->db->where("id_produto = '$data->id' AND categoria = 'promo'");
            $this->db->delete("destaque_promocao");
        }
        
        if(!isset($query[2]->categoria))
        {
            if(isset($data->lancamento))
            {
                $this->db->set("id_produto", $data->id);
                $this->db->set("categoria", "lanc");

                $this->db->insert("destaque_promocao");
            }
        }
        elseif(!isset($data->lancamento))
        {
            $this->db->where("id_produto = '$data->id' AND categoria = 'lanc'");
            $this->db->delete("destaque_promocao");
        }
        
    }


    public function get_produto()
    {
        $rst = $this->db->get("produto")->result();
       
        foreach($rst as $item)
        {
            $item->situacao = $this->get_destaque_promocao($item->id);
            $item->id_categoria = $this->get_categoria_id($item->id_categoria);
        }
        
        return $rst;
    }
    
    public function get_produto_id($id)
    {
        $rst = $this->db->get_where("produto", "id = '$id'")->row();
        
        $rst->situacao = $this->get_destaque_promocao($rst->id);
        $rst->id_categoria = $this->get_categoria_id($rst->id_categoria);
        
        return $rst;
    }

    public function get_categoria()
    {
        $rst = $this->db->get("categoria")->result();
        
        return $rst;
    }

    public function remove_produto($id)
    {
        $rst = (object)array("result" => false, "error" => "");
        
        $query = $this->db->get_where("produto_imagem", "id = $id")->result();
        
        foreach($query as $item)
        {
            $this->exclui_img($item->id, "produto_imagem");
        }
        
        $this->db->where("id_produto", $id);
        if($this->db->delete("destaque_promocao"))
        {
            $this->db->where("id_produto", $id);
            if($this->db->delete("produto_imagem"))
            {
                $this->db->where("id", $id);
                if($this->db->delete("produto"))
                {
                    $rst->result = true;
                }
                else
                {
                   $rst->error = "Erro ao tentar remover o produto, tente novamente mais tarde"; 
                }
            }
            else
            {
                $rst->error = "Erro ao tentar remover a imagem do produto, tente novamente mais tarde"; 
            }
        }
        else
        {
            $rst->error = "Erro ao tentar a situação do produto, tente novamente mais tarde"; 
        }
        
        return $rst;
    }
    /** Fim Produto */
    
    private function get_destaque_promocao($id)
    {
        $rst = $this->db->get_where("destaque_promocao", "id_produto = '$id'")->result();
        
        $info = array(0 => 0, 1 => 0, 2 => 0);
        
        foreach($rst as $item)
        {
            if($item->categoria == "dest")
            {
                $info[0] = $item;
            }
            if($item->categoria == "promo")
            {
                $info[1] = $item;
            }
            if($item->categoria == "lanc")
            {
                $info[2] = $item;
            }
        }
        
        return $info;
    }
    
    /** Item Menu */
    private function get_categoria_id($id)
    {
        $rst = $this->db->get_where("categoria", "id = '$id'")->row();
        $rst->pai = $this->get_pai_menu($rst->id_pai);
        
        return $rst;
    }

    public function get_pai_menu($id)
    {
        return $this->db->get_where("categoria", "id = '$id'")->row();
    }
    
    

    public function get_item_menu()
    {

        $query = $this->db->get_where("categoria", "id_pai = 0")->result();

        $rs = $this->get_item_menu_filho($query);

        return $rs;
    }

    public function get_item_menu_filho($data)
    {
        foreach($data as $item)
        {
            $item->filho = $this->db->get_where("categoria", "'$item->id' = id_pai")->result();
        }

        return $data;
    }

    public function get_item_menu_id($id)
    {
        $rst = $this->db->get_where("categoria", "id = $id")->row();

        $rst->pai = $this->get_pai_menu($rst->id_pai);

        return $rst;
    }

    public function salva_item_menu($data)
    {
        $rst = (object)array("result" => false, "error" => "");

        if($data->id != null)
        {
            $rst = $this->edita_item_menu($data);
        }
        else
        {
            $rst = $this->inseri_item_menu($data);
        }
        
        return $rst;
    }
    
    private function inseri_item_menu($data)
    {
        $rst = (object)array("result" => false, "error" => "");
        
        $this->db->set("id_pai", $data->marca);
        $this->db->set("nome", $data->nome);
        if($this->db->insert("categoria"))
        {
            if($data->marca == '0')
            {
                $data->id_img = $this->db->insert_id();
                $rst = $this->inseri_img_item_menu($data);
            }
            else
            {
                $rst->result = true;
            }
        }
        
        return $rst;
    }
    
    private function edita_item_menu($data)
    {
        $rst = (object)array("result" => false, "error" => "");
        
        $this->db->set("id_pai", $data->marca);
        $this->db->set("nome", $data->nome);

        $this->db->where("id", $data->id);
        if($this->db->update("categoria"))
        {
            $rst->result = true;
        }
        else
        {
            $rst->result = false;
            $rst->error = "Ocorreu um erro. Por favor tente novamente mais tarde";
        }
        
        return $rst;
    }
    
    private function inseri_img_item_menu($data)
    {
        $rst = $this->salva_img("img_lista_".$data->id_img);
        
        if($rst->result)
        {
            $this->db->set('img', $rst->error);
            $this->db->set("id_pai", $data->marca);
            $this->db->set("nome", $data->nome);

            $this->db->where("id", $data->id_img);
            if($this->db->update("categoria"))
            {
                $rst->result = true;
            }
            else
            {
                $rst->result = false;
                $rst->error = "Ocorreu um erro. Por favor tente novamente mais tarde";
            }
        }
        else
        {
            $this->db->where("id", $data->id_img);
            $this->db->delete("produto_imagem");
        }
        
        return $rst;
    }

    public function remove_item_menu($id)/* Remover a imagem junto com as coisas do banco */
    {
        $rst = (object)array("result" => false, "error" => "");

        $query = $this->db->get_where("categoria", "id_pai = '$id'")->result();

        if($query)
        {
            $this->db->where("id_pai = '$id'");
            if($this->db->delete("categoria"))
            {
                $this->db->where("id = '$id'");
                if($this->db->delete("categoria"))
                {
                    $rst->result = true;
                }
                else
                {
                    $rst->error = "Erro ao tentar remover o item, tente novamente mais tarde";
                    return $rst;
                }
            }
            else
            {
                $rst->error = "Erro ao tentar remover o item, tente novamente mais tarde";
                return $rst;
            }
        }
        else
        {
            $rst = $this->exclui_img($id, "categoria");

            if($rst->result)
            {
                $this->db->where("id = '$id'");
                if($this->db->delete("categoria"))
                {
                    $rst->result = true;
                }
                else
                {
                    $rst->error = "Erro ao tentar remover o item, tente novamente mais tarde";
                    return $rst;
                }
            }
        }

        return $rst;
    }

    public function atualiza_img_menu($data)
    {
        if($_FILES['img']['name'])
        {
            $rst = $this->salva_img("img_lista_".$data->id);
        }
        else
        {
            $rst = (object)array("result" => true, "error" => "");
        }

        if($rst->result)
        {
            $this->db->set("nome", $data->nome);
            $rst->error != "" ? $this->db->set("img", $rst->error) : "";

            $this->db->where("id", $data->id);
            if($this->db->update("categoria"))
            {
                $rst->result = true;
            }
            else
            {
                $rst->result = false;
                $rst->error = "Ocorreu um erro. Por favor tente novamente mais tarde";
            }
        }

        return $rst;
    }

    /** Fim Item Menu */

    /** Tratamento de Imagem */
    public function salva_img($name)
    {
        $rs = (object)array("result" => false, "error" => "");
        $config['upload_path']          = "./assets/img";
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['max_size']             = 10000;
        $config['max_width']            = 1920;
        $config['max_height']           = 1080;
        $config['overwrite']            = true;
        $config['file_name']            = $name;

        $this->load->library('upload');

        $this->upload->initialize($config);

        if($this->upload->do_upload('img'))
        {
            $rs->result = true;
            $rs->error = $this->upload->data("orig_name");
            return $rs;
        }
        else
        {
            $rs->error = $this->upload->display_errors();
            return $rs;
        }
    }

    public function exclui_img($id, $table)
    {
        $rst = (object)array("result" => false, "error" => "");
        $img = $this->get_name_img($id, $table);
        $ver = unlink("./assets/img/".$img);
        if($ver)
        {
            $rst->result = true;
        }
        else
        {
            $rst->result = false;
        }

        return $rst;
    }
    
    private function get_name_img($id, $table)
    {
        $rst = $this->db->get_where($table, "id = '$id'")->row();
        
        return $rst->img;
    }

    /** Fim Tratamento de Imagem */

    /** Imagem Produto */
    public function get_ultimo_id()
    {
        $this->db->select("id");
        $this->db->order_by("id", "desc");
        $rst = $this->db->get("produto_imagem")->row();

        return $rst->id;
    }

    public function salva_imagem_prod($data)
    {
        $rst = (object)array("result" => false, "error" => "");

        if($data->id_edit == '0')
        {   
            $rst = $this->inseri_img_prod($data);
        }
        else
        {
            $rst = $this->edita_img_prod($data);  
        }
        
        return $rst;
    }
    
    private function inseri_img_prod($data)
    {
        $this->db->set("id_produto", $data->id);
        $this->db->set("img", "aleatorio_name");
        if($this->db->insert("produto_imagem"))
        {
            $data->id_img = $this->db->insert_id();
            $rst = $this->edita_img_prod($data);
        }
        
        return $rst;
    }
    
    private function edita_img_prod($data)
    {   
        $rst = $this->salva_img("img_prod_".$data->id_img);
        
        if($rst->result)
        {
            $this->db->set("id_produto", $data->id);
            $this->db->set("img", $rst->error);

            $this->db->where("id", $data->id_img);
            if($this->db->update("produto_imagem"))
            {
                $rst->result = true;
            }
            else
            {
                $rst->result = false;
                $rst->error = "Erro ao tentar inserir a imagem, tente novamente mais tarde";
            }
        }
        else
        {
            $this->db->where("id", $data->id_img);
            $this->db->delete("produto_imagem");
        }
        
        return $rst;
    }

    public function get_imagens_prod($id)
    {
        $rst = $this->db->get_where("produto_imagem", "id_produto = '$id'")->result();

        return $rst;
    }

    public function remove_imagem_prod($id)
    {
        $rst = $this->exclui_img($id, "produto_imagem");
        
        if($rst->result)
        {
            $this->db->where("id", $id);
            if($this->db->delete("produto_imagem"))
            {
                $rst->result = true;
            }
            else
            {
                $rst->result = false;
                $rst->error = "Erro ao tentar remover o item, tente novamente mais tarde";
            }
        }

        return $rst;
    }

    /** Fim Imagem Produto */

    /** Contato */
    public function salva_contato($data)
    {
        $rst = (object)array("result" => false, "error" => "");

        $this->db->set("sobre", $data->sobre);
        $this->db->set("horario", $data->horario);
        $this->db->set("telefone", $data->telefone);
        $this->db->set("email", $data->email);
        $this->db->set("celular", $data->celular);
        $this->db->set("facebook", $data->facebook);
        $this->db->set("twitter", $data->twitter);
        $this->db->set("instagram", $data->instagram);
        $this->db->set("color_site", $data->color);

        $this->db->where("id", 1);
        if($this->db->update("contato"))
        {
            $rst->result = true;
        }
        else
        {
            $rst->error = "Erro ao tentar atualizar os dados de contato, tente novamente mais tarde";
        }

        return $rst;
    }

    public function get_colores()
    {
        return $this->db->get("color_site")->result();
    }

    /** Fim Contato */

    /** Formulário Carrosel */
    public function save_carrousel($data)
    {
        $rst = (object)array("result" => false, "error" => "");

        if($data->slide == 0)
        {
            $rst = $this->salva_img_carrossel($data);
        }
        else
        {
            $rst = $this->edita_img_carrossel($data);
        }
        
        return $rst;
    }
    
    private function salva_img_carrossel($data)
    {
        $this->db->set("img", "aleatorio_name");
        if($this->db->insert("carrossel_imagem"))
        {
            $data->slide = $this->db->insert_id();
            $rst = $this->edita_img_carrossel($data);
        }
        
        return $rst;
    }
    
    private function edita_img_carrossel($data)
    {
        $rst = $this->salva_img("img_carrossel_".$data->slide);
        
        if($rst->result)
        {
            $this->db->set("img", $rst->error);

            $this->db->where("id", $data->slide);
            if($this->db->update("carrossel_imagem"))
            {
                $rst->result = true;
            }
            else
            {
                $rst->result = false;
                $rst->error = "Erro ao tentar inserir a imagem, tente novamente mais tarde";
            }
        }
        else
        {
            $this->db->where("id", $data->slide);
            $this->db->delete("carrossel_imagem");
        }
        
        return $rst;
    }
    
    public function remove_imagem_carrousel($id)
    {
        $rst = $this->exclui_img($id, "carrossel_imagem");
        
        if($rst->result)
        {
            $this->db->where("id", $id);
            if($this->db->delete("carrossel_imagem"))
            {
                $rst->result = true;
            }
            else
            {
                $rst->result = false;
                $rst->error = "Erro ao tentar remover o item, tente novamente mais tarde";
            }
        }

        return $rst;
    }

    public function get_img_carrousel()
    {
        $rst = $this->db->get("carrossel_imagem")->result();

        return $rst;
    }

    public function get_ultimo_id_carrossel()
    {
        $this->db->select("id");
        $this->db->order_by("id", "desc");
        $rst = $this->db->get("carrossel_imagem")->row();
        if($rst)
            return $rst->id;
        else
            return null;
    }
    /** Fim Formulário Carrosel */
    
}
