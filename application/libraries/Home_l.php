<?php

defined('BASEPATH') OR exit('No direct script');

class Home_l extends CI_Object{
        
    public function get_itens_navbar()
    {
        $rst = $this->db->get("categoria")->result();
        
        return $rst;
    }

    public function get_item_rodape()
    {
        $rst = $this->db->get("contato")->row();

        return $rst;
    }
    
    public function get_destaque_promocao($tipo)
    {
        $this->db->limit("6");
        $rst = $this->db->get_where("destaque_promocao", "categoria = '$tipo'")->result();
        
        return $rst;
    }
    
    public function get_produto($id)
    {
        $rst = $this->db->get_where("produto", "id = '$id'")->row();
        
        return $rst;
    }
    
    public function get_categoria($id)
    {
        $query = $this->db->get_where("categoria", "id = '$id'")->row();
        
        $rst = $this->db->get_where("categoria", "id = '$query->id_pai'")->row();
        
        return $rst;
    }
    
    public function get_img_card($id)
    {
        $rst = $this->db->get_where("produto_imagem", "id_produto = '$id'")->row();
        
        return $rst;
    }
    
    public function get_lista_produto($marca, $categoria)
    {
        $query = $this->db->get_where("categoria", "nome = '$marca'")->row();
        $query2 = $this->db->get_where("categoria", "$query->id = id_pai AND nome = '$categoria'")->row();
        $rst = $this->db->get_where("produto", "id_categoria = '$query2->id'")->result();
        
        return $rst;
    }
    
    public function get_produto_id($id)
    {
        $rst = $this->db->get_where("produto", "id = $id")->row();
        
        return $rst;
    }
    
    public function get_imagem_id($id)
    {
        return $this->db->get_where("produto_imagem", "id_produto = '$id'")->result();
    }
    
    public function get_categoria_nome($nome)
    {
        $this->db->select("img, nome");
        return $this->db->get_where("categoria", "nome = '$nome'")->row();
    }
    
}