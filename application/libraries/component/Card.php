<?php 

Class Card{
    
    private $imagem;
    private $nome;
    private $preco;
    private $id;
    
    function __construct($data) {
        $this->imagem = $data["imagem"];
        $this->nome = $data["nome"];
        $this->preco = $data["preco"];
        $this->id = $data["id"];
    }
    
    public function monta_html()
    {
        return '<div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="card mb-5">
                        <div class="view overlay">
                            <img class="card-img-top card_img_a" src="'.base_url("assets/img/").$this->imagem.'" alt="Card image cap">
                            <a href="#!">
                                <div class="mask rgba-white-slight"></div>
                            </a>
                        </div>
                        <div class="card-body">
                            <h4 class="card-title">'.$this->nome.'</h4>
                            <p class="card-text">12x de R$ '.number_format($this->preco/12, 2, ',', '.').' Sem Juros.</p>
                            <p class="card-text"><span class="h3 font-weight-bold text-dark">R$ '. number_format($this->preco/1.10, 2, ',', '.').'</span> à vista com desconto no Boleto.</p>
                            <a href="'.base_url("Home/detalhes/$this->id").'" class="btn btn-danger">Ver mais</a>
                        </div>
                    </div>
                </div>';
    }
    
}