<?php

include_once APPPATH.'libraries/component/Card.php';

class Home_Model extends CI_Model{
    
    public function __construct()
    {
        parent:: __construct();
        $this->load->library("Home_l");   
    }
    
    public function get_item_rodape()
    {
        $rst = $this->home_l->get_item_rodape();

        return $rst;
    }

    public function get_item_navbar()
    {
        $rst = $this->home_l->get_itens_navbar();
        
        $option = array();
        foreach($rst as $item)
        {
            if($item->id_pai == 0)
            {
                $item->filhos = array();
                $option[] = $item;
            }
        }
        
        foreach($rst as $item)
        {
            foreach($option as $opcoes)
            {
                if($opcoes->id == $item->id_pai)
                {
                    $opcoes->filhos[] = $item;
                }
            }
        }
        
        return $option;
    }
    
    public function get_destaques_promocao($tipo)
    {
        $rst = $this->home_l->get_destaque_promocao($tipo);
        foreach($rst as $item)
        {
            $item->produto = $this->home_l->get_produto($item->id_produto);
            $item->produto->categoria = $this->home_l->get_categoria($item->produto->id_categoria);
            $item->produto->img = $this->home_l->get_img_card($item->produto->id);
            $item->card = array("imagem" => $item->produto->img->img, "nome" => $item->produto->nome, "preco" => $item->produto->preco, "id" => $item->id_produto);
        }
        
        return $rst;
    }
    
    public function get_card($data)
    {
        $html = '';
        foreach($data as $item)
        {
            $card = new Card($item->card);
            $html .= $card->monta_html();
        }
        
        return $html;
    }
    
    public function get_lista($marca, $categoria)
    {
        $rst = $this->home_l->get_lista_produto($marca, $categoria);
        
        foreach($rst as $item)
        {
            $item->img = $this->home_l->get_img_card($item->id);
            $item->card = $item->card = array("imagem" => $item->img->img, "nome" => $item->nome, "preco" => $item->preco, "id" => $item->id);
        }
        
        return $rst;
    }
    
    public function get_produto_id($id)
    {
        return $this->home_l->get_produto_id($id);
    }
    
    public function get_imagem_id($id)
    {
        return $this->home_l->get_imagem_id($id);
    }
    
    public function get_categoria_id($id)
    {
        return $this->home_l->get_categoria($id);
    }
    
    public function get_categoria_nome($id)
    {
        return $this->home_l->get_categoria_nome($id);
    }
    
}