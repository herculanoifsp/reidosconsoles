<?php

class Login_Model extends CI_Model{

    public function __construct()
    {
        parent:: __construct();
        $this->load->library("Login_l");   
    }
    

    public function acesso_login()
    {
        
        if(sizeof($_POST) == 0) return;

        if($this->validation->login_validation())
        {
            $data['nome'] = $nome = $this->input->post('login');
            $data['senha'] = $preco = $this->input->post('senha');

            return $this->login_l->check_login($data);
        }
    }

}