<?php

class Admin_Model extends CI_Model{
    
    public function __construct()
    {
        parent:: __construct();
        $this->load->library("Admin_l");   
    }
    
    /** Produto */
    public function cadastro_produto($id = null)
    {
        if(sizeof($_POST) == 0) return;

        if($this->validation->cadastro_produto_validation())
        {
            $data = (object)$this->input->post();
            $data->id = $id;
            return $this->admin_l->salva_produto($data);
        }
    }

    public function get_produto_table()
    {
        $rst  = $this->admin_l->get_produto();
        
        return $rst;
    }
    
    public function get_produto_id($id)
    {
        $rst = $this->admin_l->get_produto_id($id);
        
        return $rst;
    }
    
    public function exclui_produto($id)
    {
        $rst = $this->admin_l->remove_produto($id);
        
        return $rst;
    }
    /* Fim produto */

    /** Imagem Produto */
    public function cadastro_imagens($id)
    {
        if(sizeof($_POST) == 0) return;
        
        $data = (object)$this->input->post();
        $data->id = $id;

        return $this->admin_l->salva_imagem_prod($data);
    }

    public function get_ultimo_id()
    {
        $rst = $this->admin_l->get_ultimo_id();

        return $rst;
    }

    public function get_imagens($id)
    {
        $rst = $this->admin_l->get_imagens_prod($id);

        return $rst;
    }

    public function exclui_imagem_prod($id)
    {
        return $this->admin_l->remove_imagem_prod($id);
    }

    /** Fim Imagem Produto */
    
    public function get_categoria()
    {
        $rst = $this->admin_l->get_categoria();
        
        $option = array();
        foreach($rst as $item)
        {
            if($item->id_pai == 0)
            {
                $item->filhos = array();
                $option[] = $item;
            }
        }
        
        foreach($rst as $item)
        {
            foreach($option as $opcoes)
            {
                if($opcoes->id == $item->id_pai)
                {
                    $opcoes->filhos[] = $item;
                }
            }
        }
        
        return $option;
    }

    /** Item Menu */

    public function get_itens_menu()
    {
        $rst = $this->admin_l->get_item_menu();

        return $rst;
    }

    public function cadastro_item_menu($id = null)
    {
        if(sizeof($_POST) == 0) return;

        if($this->validation->cadastro_item_menu_validation())
        {
            $data = (object)$this->input->post();
    
            if($data->marca == '0' && empty($_FILES["img"]["name"]))
            {
                return $rst = (object)array("result" => false, "error" => "Nenhum arquivo foi selecionado");
            }
            $data->id = $id;

            return $this->admin_l->salva_item_menu($data);
        }
    }

    public function get_item_menu($id)
    {
        $rst = $this->admin_l->get_item_menu_id($id);

        return $rst;
    }

    public function exclui_item_menu($id)
    {
        $rst = $this->admin_l->remove_item_menu($id);

        return $rst;
    }

    public function atualiza_img_menu($id)
    {
        if(sizeof($_POST) == 0) return;

        if($this->validation->atualiza_img_menu_validation())
        {
            $data = (object)$this->input->post();
            $data->id = $id;

            return $this->admin_l->atualiza_img_menu($data);
        }
    }

    /** Fim Item Menu */
    
    /** Contato*/
    public function get_contato()
    {
        $rst = $this->admin_l->get_contato();
        
        return $rst;
    }
    
    public function cadastro_contato()
    {
        if(sizeof($_POST) == 0) return;

        if($this->validation->cadastro_contato_validation())
        {
            $data = (object)$this->input->post();
            return $this->admin_l->salva_contato($data);
        }
    }

    public function get_colores()
    {
        return $this->admin_l->get_colores();
    }
    /** Fim Contato */

    /** Formulário Carrosel */
    public function atualiza_carrossel()
    {
        if(sizeof($_POST) == 0) return;

        $data = (object)$this->input->post();

        return $this->admin_l->save_carrousel($data);

    }
    
    public function exclui_imagem_carrousel($id)
    {
        return $this->admin_l->remove_imagem_carrousel($id);
    }

    public function get_img_carrousel()
    {
        $rst = $this->admin_l->get_img_carrousel();

        return $rst;
    }

    /** Fim Formulário Carrosel */
}

